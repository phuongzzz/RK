//: Playground - noun: a place where people can play

import UIKit

for i in 0..<10 {
    for j in 0..<10 {
        if (i == 0) || (i == 9) {
            print("*", terminator: "")
        } else {
            if (j == 0) || (j == 9) {
                print("*", terminator: "")
            } else {
                print(" ", terminator: "")
            }
        }
    }
    print("")
}

