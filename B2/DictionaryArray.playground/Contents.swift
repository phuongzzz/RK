//: Playground - noun: a place where people can play

import UIKit

var addressBook = [[String: String]]()

var messi = [
    "name": "messi",
    "age": "31",
    "address": "barcelona",
    "phonenumber": "123456789"
]

var ronaldo = [
    "name": "ronaldo",
    "age": "33",
    "address": "real madrid",
    "phonenumber": "987654321"
]

var hazard = [
    "name": "hazard",
    "age": "26",
    "address": "chelsea",
    "phonenumber": "1029384756"
]

addressBook.append(messi)
addressBook.append(ronaldo)
addressBook.append(hazard)

let orderedAddressBook = addressBook.sorted {
    guard let a1 = $1["age"], let a2 = $0["age"] else {
        return false
    }
    return a1 > a2
}

print(orderedAddressBook)
