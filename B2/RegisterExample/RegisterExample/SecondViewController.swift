//
//  SecondViewController.swift
//  RegisterExample
//
//  Created by phuongzzz on 7/3/18.
//  Copyright © 2018 phuongzzz. All rights reserved.
//
import UIKit

class SecondViewController: UIViewController {
    var fullName = ""
    var email = ""
    var phone = ""
    
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneNumbeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fullNameLabel.text = fullName
        emailLabel.text = email
        phoneNumbeLabel.text = phone
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
