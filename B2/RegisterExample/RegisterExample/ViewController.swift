//
//  ViewController.swift
//  RegisterExample
//
//  Created by phuongzzz on 7/3/18.
//  Copyright © 2018 phuongzzz. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var phoneField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        emailField.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func submitAction(_ sender: Any) {
        if (firstNameField.text! == "" || lastNameField.text! == "" || emailField.text! == "" || phoneField.text! == "") {
            showAlert(title: "Missing fields", message: "Please fill all the fields")
        } else {
            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "secondViewController") as! SecondViewController
            secondViewController.fullName = firstNameField.text! + " " + lastNameField.text!
            secondViewController.email = emailField.text!
            secondViewController.phone = phoneField.text!
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if let emailText = emailField.text {
            if !checkEmailValid(email: emailText) {
                showAlert(title: "Invalid input", message: "Please check your email again")
            }
        }
    }

    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title,
                                      message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func checkEmailValid(email: String?) -> Bool {
        guard email != nil else { return false }
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", regEx)
        return emailTest.evaluate(with: email)
    }
}
